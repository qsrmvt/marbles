# -*- coding: utf-8 -*-
import click

from . import tome


@click.command()
@click.option("-f", "--floor", default=0)
@click.option("-x", "--xcell", default=0)
@click.option("-y", "--ycell", default=0)
@click.option("-w", "--wall", default=0)
@click.option("-s", "--shelf", default=0)
@click.option("-v", "--volume", default=0)
@click.option("-p", "--page", default=0)
def cli(floor, xcell, ycell, wall, shelf, volume, page):
    this_tome = tome.Tome(
        floor=floor,
        xcell=xcell,
        ycell=ycell,
        wall=wall,
        shelf=shelf,
        volume=volume,
    )
    click.echo(this_tome)
    click.echo(this_tome.page(page))
