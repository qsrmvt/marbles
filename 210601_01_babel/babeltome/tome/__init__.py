# -*- coding: utf-8 -*-
import random


def letter(alphabet: str = "abcdefghijklmnopqrstuvwxyz,. ") -> str:
    """Return a random character from the alphabet pool.

    Args:
        alphabet (str): Potential alphabet pool.

    Returns:
        str: A character from the alphabet pool.
    """
    return alphabet[random.randrange(0, len(alphabet))]


def check_arg(value: int, max_value: int) -> int:
    """Check a value against a range, providing a suitable random value if zero.

    Args:
        value (int): Value to check.
        max_value (int): The maximum allowed value.

    Returns:
        int: Initial value if acceptable, or a random value on zero.
    """
    if value == 0:
        return random.randrange(1, max_value)
    if value >= 1 and value <= max_value:
        return value
    print(f"Bad Value")  # @TODO: Raise an exception instead.
    exit(-1)


class Tome:
    _PROPS = {
        "floor": 1000,
        "xcell": 1000,
        "ycell": 1000,
        "wall": 4,
        "shelf": 5,
        "volume": 32,
    }
    _ROWS = 30
    _COLUMNS = 80
    _PAGES = 410

    def __init__(self, **args):
        """Initialize a new Tome object with a location, ID, and title.

        Accepts the following arguments in **kwargs, but will assign random values
        within respective ranges (specified in _PROPS) for any unspecified arguments:
            floor (int): Floor number.
            xcell (int): X cell number.
            ycell (int): Y cell number.
            wall (int): Wall number.
            shelf (int): Shelf number.
            volume (int): Volume number.

        Args:
            **args: Arbitrary keyword arguments.
        """
        for expected_arg, max_value in self._PROPS.items():
            self.__dict__[f"_{expected_arg}"] = check_arg(
                args.get(expected_arg, random.randrange(self._PROPS[expected_arg])),
                max_value,
            )
        self.id = (
            f"f{self._floor}"
            + f"x{self._xcell}"
            + f"y{self._ycell}"
            + f"w{self._wall}"
            + f"s{self._shelf}"
            + f"v{self._volume}"
        )
        random.seed(self.id)
        self.title = "".join([letter() for _ in range(random.randrange(5, 15))])

    def __str__(self) -> str:
        """Provide a string representation of this Tome.

        Returns:
            str: String representation of this Tome.
        """
        return f'Tome {self.id}: "{self.title}"'

    def page(self, page_id: int) -> str:
        """Generate a page from this Tome.

        Args:
            page_id (int): Page number to generate.

        Returns:
            str: ROWS * COLUMNS page of text from this Tome.
        """
        random.seed(self.title)
        for _ in range(self._COLUMNS * self._ROWS * (page_id - 1)):
            _ = letter()
        return "\n".join(
            [
                "".join([letter() for _ in range(self._COLUMNS)])
                for _ in range(self._ROWS)
            ]
        )
