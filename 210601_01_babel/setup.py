# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(
    name="babeltome",
    version="0.2.0",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
    ],
    entry_points={
        "console_scripts": [
            "babeltome = babeltome.main:cli",
        ],
    },
)
