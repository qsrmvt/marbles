# [qsrmvt](https://gitlab.com/qsrmvt)'s Marbles

## 20210601-01: [Tomes of Babel](./210601_01_babel/babel.py)

Inspired by the [Library of Babel](https://libraryofbabel.info/) which was in turn inspired by the
*[Library of Babel](https://en.wikipedia.org/wiki/The_Library_of_Babel)*, this small bit of code
will pseudo-randomly generate pages for the texts in the Library.

Each tome is identified by a floor, x cell, y cell, wall, shelf, and volume. Although the program
doesn't store more than a page of text at any given time, providing the same identification on
different occasions should result in the same pages being generated.